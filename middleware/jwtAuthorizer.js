const firebaseAdmin = require('../services/firebaseAdminService');
const services = require('../services');

function getPathInfo(pathName) {
    return new Promise((resolve, reject) => {
        services.forEach((s) => {
            let found = s.paths.find((p) => {
                return new RegExp(p.regex).test(pathName);
            });
            if (found) {
                return resolve(found);
            }
        });
        reject();
    });
}

function getIdToken(req) {
    return new Promise((resolve, reject) => {
        let idToken = req.headers['x-access-token'] || req.headers['authorization'];
        if (!idToken || !idToken.startsWith('Bearer ')) {
            reject();
        } else {
            resolve(idToken.slice(7, idToken.length));
        }
    });
}

module.exports = (req, res, next) => {
    getPathInfo(req.path).then((pathInfo) => {
        if (!pathInfo.jwt) {
            return next();
        }
        getIdToken(req).then((idToken) => {
            firebaseAdmin.auth().verifyIdToken(idToken).then((decodedToken) => {
                req.headers['uid'] = decodedToken.uid;
                return next();
            }).catch((e) => {
                console.log(e);
                return res.status(401).json();
            })
        }).catch((e) => {
            console.log(e);
            return res.status(401).json();
        })
    }).catch(() => {
        console.log('404');
        return res.status(404).json();
    });
};
