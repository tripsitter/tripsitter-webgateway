module.exports = require('express-rate-limit')({
    windowMs: 60 * 1000,
    max: 200,
    message: 'You have exceeded the 200 requests in past minute',
    headers: true,
});
