const services = require('../services');
let proxy = require('express-http-proxy');

module.exports = {
    getServices: () => {
        return services;
    },
    routes: (app) => {
        services.forEach((service) => {
            console.log(`---------- Service: ${service.name} ----------`);
            service.paths.forEach((path) => {
                if(service.use_proxy === "true" && !process.env.deploy) {
                    console.log(`Path: https://gateway.tripsitter.dev${path.path} | Regex: ${path.regex}`);
                    app.all(path.path, proxy('https://gateway.tripsitter.dev'));
                } else {
                    console.log(`Path: ${process.env[service.name]}${path.path} | Regex: ${path.regex}`);
                    app.all(path.path, proxy(process.env[service.name]))
                }

            });
        });
    }
};
