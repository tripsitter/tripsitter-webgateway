const firebaseAdmin = require('firebase-admin');
const serviceAccount = require("../private/tripsitter-58727-firebase-adminsdk-kqp9b-631c4e92fb");

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://tripsitter-58727.firebaseio.com"
});

module.exports = firebaseAdmin;
