const app = require('express')();
const port  = process.env.PORT || 3000;

app.use(require('cors')({
    origin:  ['http://localhost:8080', 'https://tripsitter.dev'],
}));

app.use(require('./middleware/rateLimiter'));

require('dotenv').config();

app.use(require('./middleware/jwtAuthorizer'));
require('./services/servicesService').routes(app);

app.listen(port, () => console.log(`Web gateway is running on ${port}`));
